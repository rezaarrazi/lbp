import cv2
import os
import random
import numpy as np
from PIL import Image
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
image_dir = os.path.abspath(os.path.join(BASE_DIR, os.pardir, "datasetbola"))
positif_image_dir = os.path.join(image_dir, "positif")

print(positif_image_dir)

current_id = 0
label_ids = {}
y_labels = []
x_train = []

text_file = open("positives.txt", "w")

def load_image_positif(image_dir):
    print(image_dir)
    for root, dirs, files in os.walk(image_dir):
        for img_dir in dirs:
            print(img_dir)
            for sroot, sdirs, sfiles in os.walk(os.path.join(positif_image_dir, img_dir)):
                for file in sfiles:
                    if file.endswith("png") or file.endswith("jpg"):
                        path = os.path.join(os.pardir, "datasetbola", "positif", img_dir, file)
                        # print(path)
                        # path = os.path.join(root, file)
                        pil_image = Image.open(path)
                        width, height = pil_image.size
                        text_file.write("%s 1 0 0 %d %d\n" % (path, width, height))

load_image_positif(positif_image_dir)