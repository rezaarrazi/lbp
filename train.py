import cv2
import os
import random
import numpy as np
from PIL import Image
import pickle

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
image_dir = os.path.abspath(os.path.join(BASE_DIR, os.pardir, "datasetbola"))
positif_image_dir = os.path.join(image_dir, "positif")
negatif_image_patch_dir = os.path.join(image_dir, "negatif", "_negatif")
negatif_image_dir = os.path.join(image_dir, "negatif", "neg_top")

# print(positif_image_dir)
# print(negatif_image_dir)

# face_cascade = cv2.CascadeClassifier('cascades/data/haarcascade_frontalface_alt2.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()

current_id = 0
label_ids = {}
y_labels = []
x_train = []

def load_image_positif(image_dir):
    print(image_dir)
    for root, dirs, files in os.walk(image_dir):
        for file in files:
            if file.endswith("png") or file.endswith("jpg"):
                path = os.path.join(root, file)
                # print(label, path)

                pil_image = Image.open(path).convert("L") # grayscale
                size = (50, 50)
                final_image = pil_image.resize(size, Image.ANTIALIAS)
                image_array = np.array(final_image, "uint8")
                x_train.append(image_array)
                y_labels.append(1)

load_image_positif(positif_image_dir)

def load_image_patch_negatif(image_dir):
    print(image_dir)
    for root, dirs, files in os.walk(image_dir):
        for file in files:
            if file.endswith("png") or file.endswith("jpg"):
                path = os.path.join(root, file)
                # print(label, path)

                pil_image = Image.open(path).convert("L") # grayscale
                size = (50, 50)
                final_image = pil_image.resize(size, Image.ANTIALIAS)
                image_array = np.array(final_image, "uint8")
                x_train.append(image_array)
                y_labels.append(0)

load_image_patch_negatif(negatif_image_patch_dir)

def load_image_negatif(image_dir):
    print(image_dir)
    for root, dirs, files in os.walk(image_dir):
        for file in files:
            if file.endswith("png") or file.endswith("jpg"):
                path = os.path.join(root, file)
                # print(label, path)

                pil_image = Image.open(path).convert("L") # grayscale
                image_array = np.array(pil_image, "uint8")
                size = (50, 50)
                
                img_size = image_array.shape
                x = random.randint(0, img_size[0]-size[0])
                y = random.randint(0, img_size[1]-size[1])
                roi = image_array[y:y+size[0], x:x+size[1]]
                if (roi.shape[0] >= size[0]) and (roi.shape[1] >= size[1]):
                    x_train.append(roi)
                    y_labels.append(0)

load_image_negatif(negatif_image_dir)

# print(y_labels)
# print(x_train)

recognizer.train(x_train, np.array(y_labels))
recognizer.save("ball-trainner2.yml")